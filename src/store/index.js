import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

// See https://vuex.vuejs.org/en/plugins.html
const logger = createLogger({
  collapsed: false // auto-expand logged mutations
})

const isProduction = process.env.NODE_ENV === 'production'

export default new Vuex.Store({
  state: {},
  modules: {},
  strict: !isProduction,
  plugins: isProduction ? [] : [logger]
})
