import Vue from 'vue'

import Router from 'vue-router'
import router from './router/index'

import store from './store/index'

import axios from 'axios'
import VueAxios from 'vue-axios'

import App from './App.vue'
import './registerServiceWorker'

Vue.use(Router)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
